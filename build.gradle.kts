plugins {
	kotlin("multiplatform") version "1.6.10"
}

group = "com.github.doomsdayrs.lib"
version = "1.0-SNAPSHOT"

val applicationId = "org.gtk-kt.example"

repositories {
	mavenCentral()
	mavenLocal()
}

kotlin {
	linuxX64("native") {
		val main by compilations.getting
		binaries {
			executable()
		}
	}

	sourceSets {

		val nativeMain by getting {
			dependencies {
				implementation("org.gtk-kt:gtk:+")
				implementation("org.gtk-kt:dsl:+")
				implementation("org.gtk-kt:ktx:+")
				implementation("org.gtk-kt:coroutines:+")
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.0-native-mt")
				implementation("org.gnome.libadwaita:libadwaita-kt:+")
			}
		}
	}
}
