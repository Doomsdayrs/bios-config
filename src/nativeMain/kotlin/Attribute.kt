/**
 * 15 / 01 / 2022
 *
 * @see <a href=""></a>
 */
sealed class Attribute {
	abstract class GenericAttribute<T> : Attribute() {
		abstract val currentValue: T
		abstract val defaultValue: T?
		abstract val displayName: String
		abstract val displayNameLanguageCode: String?
		abstract val dellModifier: String?
	}

	data class EnumerationType(
		override val currentValue: String,
		override val defaultValue: String?,
		override val displayName: String,
		override val displayNameLanguageCode: String?,
		val possibleValues: List<String>,
		override val dellModifier: String? = null,
		val dellValueModifier: String? = null
	) : GenericAttribute<String>()

	data class IntegerType(
		override val currentValue: Int,
		override val defaultValue: Int?,
		override val displayName: String,
		override val displayNameLanguageCode: String?,
		val range: IntRange,
		val scalarIncrement: Int,
		override val dellModifier: String? = null
	) : GenericAttribute<Int>()

	data class StringType(
		override val currentValue: String,
		override val defaultValue: String?,
		override val displayName: String,
		override val displayNameLanguageCode: String?,
		val maxLength: Int,
		val minLength: Int,
		override val dellModifier: String? = null
	) : GenericAttribute<String>()

	/**
	 * A read-only attribute reads 1 if a reboot is necessary to apply
	 * pending BIOS attribute changes. Also, an uevent_KOBJ_CHANGE is
	 * generated when it changes to 1.
	 */
	data class PendingReboot(val isPendingReboot: Boolean) : Attribute()

	/**
	 * This attribute can be used to reset the BIOS Configuration.
	 *
	 * Specifically, it tells which type of reset BIOS configuration is being
	 * requested on the host.
	 */
	data class ResetBios(
		val selected: Int,
		val options: List<String>
	) : Attribute()

	/**
	 * This write only attribute can be used to send debug commands to the BIOS.
	 *
	 * This should only be used when recommended by the BIOS vendor.
	 *
	 * Vendors may use it to enable extra debug attributes or BIOS features for
	 * testing purposes.
	 *
	 * Note that any changes to this attribute requires a reboot for changes to
	 * take effect.
	 */
	object DebugCmd : Attribute()
}
