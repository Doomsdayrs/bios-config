/**
 * 15 / 01 / 2022
 *
 * 	Devices support various authentication mechanisms which can be exposed
 * 	as a separate configuration object.
 *
 * 	For example a "BIOS Admin" password and "System" Password can be set,
 * 	reset or cleared using these attributes.
 * 	- An "Admin" password is used for preventing modification to the BIOS
 * 	settings.
 * 	- A "System" password is required to boot a machine.
 *
 * 	Change in any of these two authentication methods will also generate an
 * 	uevent KOBJ_CHANGE.
 */
interface IAuthentication {
	val isEnabled: Boolean
	val role: String
	val mechanism: String

	val maxPasswordLength: Int
	val minPasswordLength: Int

	var currentPassword: String
	var newPassword: String
}