/**
 * 15 / 01 / 2022
 *
 * @see <a href=""></a>
 */
interface IFirmwareDevice {
	val name: String

	val attributes: List<Attribute>

	val auth: IAuthentication

	/**
	 * @throws UnsupportedOperationException if there is no such option
	 */
	@Throws(UnsupportedOperationException::class)
	fun writeDebugCMD(cmd: String)
}