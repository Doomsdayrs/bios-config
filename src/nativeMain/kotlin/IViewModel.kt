import kotlinx.coroutines.flow.Flow

/**
 * 15 / 01 / 2022
 *
 * @see <a href=""></a>
 */
interface IViewModel {
	val devices: Flow<List<IFirmwareDevice>>

	suspend fun loadDevices()
}