import kotlinx.coroutines.flow.MutableStateFlow
import org.gtk.gio.File
import org.gtk.gio.FileInfo
import org.gtk.gio.ktx.FileIterator.Companion.iterator
import org.gtk.glib.KGBytes
import org.gtk.glib.KGError
import org.gtk.glib.bool
import org.gtk.glib.use

/**
 * 15 / 01 / 2022
 *
 * @see <a href=""></a>
 */
class ImplViewModel : IViewModel {

    override val devices: MutableStateFlow<List<IFirmwareDevice>> by lazy {
        MutableStateFlow(listOf())
    }

    /**
     * @param attributeFile
     * /sys/class/firmware-attributes/DEVICE/attributes/ATTRIBUTE/
     */
    private fun createAttribute(
        attributeFile: File,
        fileInfo: FileInfo
    ): Attribute {
        println("Reading: ${fileInfo.name}")
        return when (fileInfo.name) {
            "pending_reboot" -> {
                val value = attributeFile.loadContents()!!
                println("\tValue: [$value]")
                Attribute.PendingReboot(
                    value.toInt().bool
                )
            }
            "reset_bios" -> {
                val contents = attributeFile.loadContents()!!

                val selected = contents.substring(
                    contents.indexOf("["),
                    contents.indexOf("]")
                )

                val options = contents
                    .replace("[", "")
                    .replace("]", "")
                    .split(" ")

                Attribute.ResetBios(
                    options.indexOf(selected),
                    options
                )

            }
            "debug_cmd" ->
                Attribute.DebugCmd

            else -> {
                val childrenEnum = attributeFile.enumerateChildren()

                val childrenInfo = childrenEnum
                    .iterator()
                    .toList()

                val children = childrenInfo.map { it.name }

                val childrenFiles = childrenInfo.map {
                    childrenEnum.getChild(it)
                }

                fun getValue(name: String): String? {
                    val index = children.indexOf(name)
                    return if (index != -1)
                        childrenFiles[index].loadContents()
                    else null
                }


                val type =
                    getValue("type")
                println("\ttype: $type")

                val currentValue =
                    getValue("current_value")!!
                println("\tcurrentValue: $currentValue")

                val defaultValue =
                    getValue("default_value")
                println("\tdefaultValue: $defaultValue")

                val displayName =
                    getValue("display_name")!!
                println("\tdisplayName: $displayName")

                val displayNameLanguageCode =
                    getValue("display_name_language_code")
                println("\tdisplayNameLanguageCode: $displayNameLanguageCode")


                when (type) {
                    "integer" -> {
                        println("integer")
                        createIntegerAttribute(
                            currentValue,
                            defaultValue,
                            displayName,
                            displayNameLanguageCode,
                            ::getValue
                        )
                    }
                    "string" -> {
                        println("string")
                        createStringType(currentValue, defaultValue, displayName, displayNameLanguageCode, ::getValue)

                    }
                    else -> { // "enumeration"
                        println("enum")
                        createEnumType(currentValue, defaultValue, displayName, displayNameLanguageCode, ::getValue)
                    }
                }
            }
        }
    }

    private fun createStringType(
        currentValue: String,
        defaultValue: String?,
        displayName: String,
        displayNameLanguageCode: String?,
        getValue: (String) -> String?
    ): Attribute {
        val minLength =
            getValue("min_length")!!.toInt()

        val maxLength =
            getValue("max_length")!!.toInt()

        return Attribute.StringType(
            currentValue,
            defaultValue,
            displayName,
            displayNameLanguageCode,
            maxLength,
            minLength,
        )
    }

    private fun createEnumType(
        currentValue: String,
        defaultValue: String?,
        displayName: String,
        displayNameLanguageCode: String?,
        getValue: (String) -> String?
    ): Attribute {
        val possibleValues =
            getValue("possible_values")!!.let {
                if (it.contains(";"))
                    it.split(";")
                else it.split(",")
            }

        return Attribute.EnumerationType(
            currentValue,
            defaultValue,
            displayName,
            displayNameLanguageCode,
            possibleValues,
        )
    }

    private fun createIntegerAttribute(
        currentValue: String,
        defaultValue: String?,
        displayName: String,
        displayNameLanguageCode: String?,
        getValue: (String) -> String?
    ): Attribute {
        val minValue =
            getValue("min_value")!!.toInt()

        val maxValue =
            getValue("max_value")!!.toInt()

        val scalarIncrement =
            getValue("scalar_increment")!!.toInt()

        return Attribute.IntegerType(
            currentValue.toInt(),
            defaultValue?.toInt(),
            displayName,
            displayNameLanguageCode,
            IntRange(minValue, maxValue),
            scalarIncrement
        )
    }

    /**
     * @param dir /sys/class/firmware-attributes/DEVICE/
     */
    private fun createAttributes(dir: File): List<Attribute> =
        File.newForPath(dir.path!! + "/attributes/").use { file ->
            val enumerator = file.enumerateChildren()

            enumerator.iterator()
                .filterNotNull()
                .map { fileInfo ->
                    createAttribute(
                        enumerator.getChild(fileInfo),
                        fileInfo
                    )
                }
        }

    /**
     * @param deviceDir /sys/class/firmware-attributes/DEVICE/
     */
    private fun createDevice(
        deviceFileInfo: FileInfo,
        deviceDir: File
    ): IFirmwareDevice {
        println("# creating device ${deviceFileInfo.name}")
        return object : IFirmwareDevice {
            override val name: String =
                deviceFileInfo.name ?: "Unknown"

            override val attributes: List<Attribute> =
                createAttributes(deviceDir)

            override val auth: IAuthentication
                get() = TODO("Not yet implemented")

            override fun writeDebugCMD(cmd: String) {
                File.newForPath(deviceDir.path!! + "/debug_cmd")
                    .use { file ->
                        val output = file.appendTo()
                        val byteArray = cmd.encodeToByteArray()
                        output.writeBytes(
                            KGBytes(
                                Array(byteArray.size) {
                                    byteArray[it].toUByte()
                                }
                            )
                        )
                        output.close()
                    }
            }

            override fun toString(): String {
                return "FirmwareDevice(name='$name',attributes=$attributes)"
            }
        }
    }

    override suspend fun loadDevices() {
        File.newForPath(MAIN_DIR_PATH).use { mainDir ->
            try {
                val enumerator = mainDir.enumerateChildren()

                devices.tryEmit(
                    enumerator.iterator().mapNotNull { childFileInfo ->
                        if (childFileInfo.name != null)
                            createDevice(
                                childFileInfo,
                                enumerator.getChild(childFileInfo)
                            )
                        else null
                    }
                )
            } catch (e: KGError) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        private const val MAIN_DIR_PATH = "/sys/devices/virtual/firmware-attributes/"
    }
}