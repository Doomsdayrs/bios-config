import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.gnome.gtkx.coroutines.launchIO
import org.gnome.libadwaita.adwaita.ApplicationWindow
import org.gtk.dsl.gio.onCreateUI
import org.gtk.dsl.gtk.application
import org.gtk.dsl.gtk.onClicked
import org.gtk.gtk.widgets.button.Button

fun main(){
	val viewModel: IViewModel by lazy {
		ImplViewModel()
	}

	launchIO {
		viewModel.devices.collect {
			println(it)
		}
	}

	runBlocking {
		viewModel.loadDevices()
	}
}

fun main2() {
	application("org.gtk-kt.BIOSConfig") {
		val viewModel: IViewModel by lazy {
			ImplViewModel()
		}

		onCreateUI {
			val window = ApplicationWindow(this)

			launchIO {
				viewModel.devices.collect {
					println(it)
				}
			}

			window.content = Button("List").apply {
				onClicked {
					launchIO {
						viewModel.loadDevices()
					}
				}
			}

			window.show()
		}
	}
}